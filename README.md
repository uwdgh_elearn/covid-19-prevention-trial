COVID-19 Prevention Trial child-theme using the UW Boundless WordPress theme.

This theme requires the UW 2014 WordPress theme:
https://github.com/uweb/uw-2014

The UW Slideshow template that is part of this child-theme requires the uw-slideshow plugin:
https://github.com/uweb/uw-slideshow

This theme was forked from the [DGH Boundless theme](https://bitbucket.org/uwdgh_elearn/dgh-uw-boundless-wordpress-theme) with tag 0.4.6

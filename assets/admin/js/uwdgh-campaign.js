
var _redcaplink = '';
var _event_category = '';
var _event_label = '';

function _setContext(type, targettitle) {
  switch (type) {
    case 'pre-screening':
      _redcaplink = 'https://redcap.fredhutch.org/surveys/?s=TYLX3JKEN9';
      _event_category = 'Survey Link';
      _event_label = targettitle;
      break;
    case 'provider-referral':
      _redcaplink = 'https://redcap.fredhutch.org/surveys/?s=RXTXY9F34L';
      _event_category = 'Survey Link';
      _event_label = targettitle;
      break;
    case 'results':
      // _redcaplink = 'https://redcap.iths.org/surveys/?s=3NYTKAMWL4';
      // _event_category = 'Survey Link';
      // _event_label = targettitle;
      break;
  }
}

/**
 * doClickEvent
 */
function doClickEvent(type, targettitle) {

  _setContext(type, targettitle);

  gtag('event', 'click', {
    'event_category': _event_category,
    'event_label': _event_label
  });
  return gtag_report_conversion(_redcaplink);

  window.open(_redcaplink, '_blank');

}

jQuery(document).ready(function( $ ) {

  $(document).on("click", ".btn-covid19prevention--pre-screening", function(e){
    e.preventDefault();
    doClickEvent('pre-screening', e.target.title);
  });

  $(document).on("click", ".btn-covid19prevention--provider-referral", function(e){
    e.preventDefault();
    doClickEvent('provider-referral', e.target.title);
  });

  $(document).on("click", ".btn-covid19prevention--results", function(e){
    e.preventDefault();
    doClickEvent('results', e.target.title);
  });

})

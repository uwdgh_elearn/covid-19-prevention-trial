<?php
/**
 * Template Name: Video Hero
 */
?>

<?php get_header();
  $vid = esc_attr( get_post_meta($post->ID, "video_hero", true) );
  $video_byline = esc_attr( get_post_meta($post->ID, "video_hero_byline", true) );
  $button_url = esc_url_raw( get_post_meta($post->ID, "video_hero_link_url", true) );
  $button_text = esc_attr( get_post_meta($post->ID, "video_hero_link_button_text", true) );
  ($button_text) ? null : $button_text = 'Read more' ;
  $sidebar = get_post_meta($post->ID, "sidebar");
?>

<div class="mobile-menu-wrapper">
  <?php get_template_part( 'menu', 'mobile' ); ?>
</div>

<div class="uw-hero-image hero-height">
  <div class="uw-hero-video">
    <video autoplay="" loop="" muted="" playsinline="" class="media" data-object-fit="" poster="">
      <source src="<?php echo wp_get_attachment_url($vid) ?>" type="video/mp4">
    </video>
  </div>
  <div id="hero-bg">
    <div id="hero-container" class="container">
      <h1 class="uw-site-title"><?php the_title(); ?></h1>
      <span class="udub-slant"><span></span></span>
      <div class="video-byline"><?php echo $video_byline; ?></div>
      <?php if ($button_url) : ?>
      <a class="uw-btn btn-sm btn-none" href="<?php echo $button_url; ?>"><?php echo $button_text; ?></a>
      <?php endif; ?>
    </div>
  </div>
</div>


<div class="container uw-body">

  <div class="row">

    <div class="col-md-<?php echo (($sidebar[0]!="on") ? "8" : "12" ) ?> uw-content" role='main'>

      <?php //uw_site_title(); ?>
      <?php get_template_part( 'breadcrumbs' ); ?>

      <div id='main_content' class="uw-body-copy" tabindex="-1">


        <?php
          // Start the Loop.
          while ( have_posts() ) : the_post();

            //the_content();
            get_template_part( 'content', 'page-noheader' );

            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) {
              comments_template();
            }

          endwhile;
        ?>

      </div>

    </div>

    <div id="sidebar"><?php
      if($sidebar[0]!="on"){
        get_sidebar();
      }
    ?></div>

  </div>

</div>

<?php get_footer(); ?>

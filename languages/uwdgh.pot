# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR University of Washington
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: uwdgh 0.3.2\n"
"Report-Msgid-Bugs-To: https://depts.washington.edu/dghweb/\n"
"POT-Creation-Date: 2020-05-28 16:58-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: content-page.php:18
msgid "Close Menu"
msgstr ""

#: footer.php:3 footer.php:47 thinstrip.php:4 thinstrip.php:5
msgid "University of Washington"
msgstr ""

#: footer.php:3 functions.php:251 thinstrip.php:5
msgid "Department of Global Health"
msgstr ""

#: footer.php:5
msgid "Be boundless"
msgstr ""

#: footer.php:7
msgid "Connect with us"
msgstr ""

#: footer.php:20 functions.php:221 functions.php:223
msgid "Contact Us"
msgstr ""

#: footer.php:21 footer.php:32
msgid "Jobs"
msgstr ""

#: footer.php:22
msgid "Events"
msgstr ""

#: footer.php:23
msgid "News"
msgstr ""

#: footer.php:24
msgid "Intranet"
msgstr ""

#: footer.php:25
msgid "Donate"
msgstr ""

#: footer.php:30
msgid "Accessibility"
msgstr ""

#: footer.php:31
msgid "Contact the UW"
msgstr ""

#: footer.php:33
msgid "Campus Safety"
msgstr ""

#: footer.php:34
msgid "My UW"
msgstr ""

#: footer.php:35
msgid "Rules Docket"
msgstr ""

#: footer.php:36
msgid "Privacy"
msgstr ""

#: footer.php:37
msgid "Terms"
msgstr ""

#: footer.php:42
msgid "Site managed by DGHweb"
msgstr ""

#: functions.php:32
msgid "Theme Options"
msgstr ""

#: functions.php:33
msgid "Here you can manage options for the child-theme."
msgstr ""

#: functions.php:40
msgid "Hide the title on the front page"
msgstr ""

#: functions.php:51
msgid "Enable Back-to-Top link"
msgstr ""

#: functions.php:62
msgid "Override quicklinks styling"
msgstr ""

#: functions.php:68
msgid ""
"The UW-2014 theme has styling erroneously hardcoded for the 6th and 7th "
"items in the quicklinks menu (Facebook and Twitter icons)."
msgstr ""

#: functions.php:69
msgid ""
"This option overrides the quicklinks css and creates uniform menu items for "
"all links in a custom menu added to the Quick Links display location."
msgstr ""

#: functions.php:75
msgid "Show the \"development\" banner"
msgstr ""

#: functions.php:81
msgid "Banner text"
msgstr ""

#: functions.php:195
msgid "Thinstrip"
msgstr ""

#: functions.php:216
msgid "Support Us"
msgstr ""

#: functions.php:218
msgid "Donate to Global Health"
msgstr ""

#: functions.php:261
msgid "Search results for"
msgstr ""

#: functions.php:267
msgid "Author"
msgstr ""

#: functions.php:376
msgid "Slideshow shortcode"
msgstr ""

#: functions.php:378
msgid "No Page Title"
msgstr ""

#: functions.php:379
msgid "Go to Slideshows"
msgstr ""

#: functions.php:405
msgid ""
"The <a href=\"https://github.com/uweb/uw-slideshow\" target=\"_blank\">uw-"
"slideshow</a> plugin is required with the current template."
msgstr ""

#: functions.php:416
msgid "Log out"
msgstr ""

#: functions.php:418
msgid "Log in"
msgstr ""

#: header.php:32
msgid "Skip to main content"
msgstr ""

#: thinstrip.php:42
msgid "Open quick links"
msgstr ""

#: thinstrip.php:42
msgid "Quick Links"
msgstr ""

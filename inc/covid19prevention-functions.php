<?php
/**
* Large campaign buttons shortcode
* e.g.: [covid19campaign_button class="uwdgh-dialog--join-study" title="Join The Study"]Join The Study[/covid19campaign_button]
*/
function uwdgh_covid19campaign_button($atts = [], $content) {
  ( $atts['url'] ) ? $url = $atts['url'] : $url = '#' ;
  ( $atts['new_window']=='yes' ) ? $target = '_blank' : $target = '_self' ;
  ( $atts['title'] ) ? $title = $atts['title'] : $title = $content ;
  ( $atts['class'] ) ? $addedClass = ' '.$atts['class'] : $addedClass = '' ;

  $output = '<a class="uw-btn btn-covid19campaign' . $addedClass . '" title="' . $title . '" href="' . $url . '" target="' . $target . '">' . $content .  '</a>';
  if ($atts['class']=='uwdgh-dialog--join-study') {
    $output .= '<noscript><!--//noscript fallback for JOIN THE STUDY//--><p><small>If the <samp style="font-weight: 600; text-transform: uppercase;">Join The Study</samp> button does not appear to work, please use this link: <a href="https://redcap.iths.org/surveys/?s=MA7C8PX4RR" title="Join The Study" target="_blank">Join The Study</a></small></p></noscript>';
  } elseif ($atts['class']=='uwdgh-dialog--provider-referral') {
    $output .= '<noscript><!--//noscript fallback for PROVIDER REFERRAL//--><p><small>If the <samp style="font-weight: 600; text-transform: uppercase;">Provider Referral</samp> button does not appear to work, please use this link: <a href="https://redcap.iths.org/surveys/?s=MA7C8PX4RR" title="Provider Referral" target="_blank">Provider Referral</a></small></p></noscript>';
  }

  return $output;
}
// Add shortcode
add_shortcode('covid19campaign_button', 'uwdgh_covid19campaign_button');

/**
 * add custom js for campaign
 */
function uwdgh_enqueue_campaign_script() {
  wp_register_script('uwdgh-campaign', get_stylesheet_directory_uri() . '/assets/admin/js/uwdgh-campaign.js', array('jquery'));
  wp_enqueue_script('uwdgh-campaign');
}
add_action( 'wp_print_scripts', 'uwdgh_enqueue_campaign_script', 99 );
